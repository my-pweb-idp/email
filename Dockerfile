# FROM openjdk:16-jdk-alpine

# COPY . /usr/src/hr-app-email

# ARG envir=dev
# ENV envValue=$envir

# WORKDIR /usr/src/hr-app-email

# RUN apk upgrade --update \
# 	&& apk add -U tzdata \
# 	&& cp /usr/share/zoneinfo/Europe/Bucharest /etc/localtime

# RUN ["chmod", "+x", "hr-app-email-start.sh"]

# CMD ["sh", "-c", "./hr-app-email-start.sh ${envValue}"]


FROM maven:3.8.2-jdk-11-openj9

WORKDIR /usr/src/email

COPY . .

RUN mvn clean install

CMD mvn spring-boot:run