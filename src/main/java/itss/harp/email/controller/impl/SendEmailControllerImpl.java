package itss.harp.email.controller.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.controller.SendEmailController;
import itss.harp.email.domain.EmailRequest;
import itss.harp.email.domain.Response;
import itss.harp.email.service.impl.SendEmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class SendEmailControllerImpl implements SendEmailController {

    @Autowired
    SendEmailServiceImpl emailServiceImpl;

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public ResponseEntity<Response> sendEmail(EmailRequest emailRequest) throws IOException {

        return emailServiceImpl.sendEmail(emailRequest, getTemplateName(emailRequest));
    }

    @Override
    public void sendEmailRabbit(EmailRequest emailRequest) throws IOException {
        System.out.println("Message " + emailRequest.getEmails());
        emailServiceImpl.sendEmail(emailRequest, getTemplateName(emailRequest));
    }

    private String getTemplateName(EmailRequest emailRequest) {
        if(emailRequest.getTemplateName().equals(templateConfig.getApprovedHoliday().getName()))
            return templateConfig.getApprovedHoliday().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getRejectedHoliday().getName()))
            return templateConfig.getRejectedHoliday().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getSubmitHoliday().getName()))
            return templateConfig.getSubmitHoliday().getType();

        if (emailRequest.getTemplateName().equals(templateConfig.getDaysFromPreviousYear().getName()))
            return templateConfig.getDaysFromPreviousYear().getType();

        if (emailRequest.getTemplateName().equals(templateConfig.getManagerTlnotification().getName()))
            return templateConfig.getManagerTlnotification().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getEditHoliday().getName()))
            return templateConfig.getEditHoliday().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getEditExtraRecipients().getName()))
            return templateConfig.getEditExtraRecipients().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getDeleteHoliday().getName()))
            return templateConfig.getDeleteHoliday().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getApproveDeletion().getName()))
            return templateConfig.getApproveDeletion().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getRejectDeletion().getName()))
            return templateConfig.getRejectDeletion().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getHolidayDeletionAnnouncement().getName()))
            return templateConfig.getHolidayDeletionAnnouncement().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getAddBonusDay().getName()))
            return templateConfig.getAddBonusDay().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getAdminSubmit().getName()))
            return templateConfig.getAdminSubmit().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getAdminDelete().getName()))
            return templateConfig.getAdminDelete().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getAdminEdit().getName()))
            return templateConfig.getAdminEdit().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getModifiedFiscalDate().getName()))
            return templateConfig.getModifiedFiscalDate().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getApprovedHolidayToOfficeManager().getName()))
            return templateConfig.getApprovedHolidayToOfficeManager().getType();

        if(emailRequest.getTemplateName().equals(templateConfig.getChangedProfileInfo().getName()))
            return templateConfig.getChangedProfileInfo().getType();

        throw new RuntimeException();
    }
}
