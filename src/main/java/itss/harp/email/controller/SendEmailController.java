package itss.harp.email.controller;

import itss.harp.email.domain.EmailRequest;
import itss.harp.email.domain.Response;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;

public interface SendEmailController {
    @PostMapping(value = "/sendEmail",
            produces = {MediaType.APPLICATION_JSON_VALUE},
            consumes = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<Response> sendEmail(@RequestBody EmailRequest emailRequest) throws IOException;


    @RabbitListener(queues = {"${queue.name}"})
    void sendEmailRabbit(EmailRequest emailRequest) throws IOException;

}