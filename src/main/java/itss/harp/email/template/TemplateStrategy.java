package itss.harp.email.template;

public interface TemplateStrategy {

    String write(String email, String... args);

    String getType();

    default String getFirstName(String email) {
        String name = email.split("@")[0];
        String firstName = name.split("\\.")[0];
        return firstName.substring(0, 1).toUpperCase() + firstName.substring(1);
    }

    default String getLastName(String email) {
        String name = email.split("@")[0];
        String lastName = name.split("\\.")[1];
        return lastName.substring(0, 1).toUpperCase() + lastName.substring(1);
    }

}
