package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class MinVacationDaysAvailableTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Draga " + "<span style=\"color: #03a4ef\">" + getFirstName(email) +  "</span>" + "," + "</head>";
        String body = "<body>\n" +
                "<p> Se pare ca ai avut un an plin si ai uitat sa te mai si relaxezi. Uite ca am ajuns in " + args[0]+
                " si ai reusit sa strangi un numar frumos de zile de concediu care asteapta sa fie luate :) ." +
                "In acest moment mai ai de utilizat " + "<span style=\"color: #03a4ef\">" +  args[1] + "</span>" +" zile de concediu. </p>" +
                "<p> Ne bucura dedicarea ta, insa ne ingrijoreaza lipsa de relaxare, asa ca te incurajam calduros sa te folosesti de ele si sa iti reincarci bateriile :)." +
                " Ne asteapta un nou an in fata cu provocari la fel de frumoase.</p>" +
                "<p>Spor la planificat concediu, asta e cea mai simpla planificare, insa daca ai nevoie de ajutor sau idei suntem aici:)</p>" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</body>\n";

        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-Holidays_notification.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + "</th>" +"</tr>"+ "</table>" +"</html>";

    }

    @Override
    public String getType() {
        return templateConfig.getMinVacationDaysAvailable().getName();
    }




}
