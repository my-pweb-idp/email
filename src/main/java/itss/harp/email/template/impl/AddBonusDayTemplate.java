package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component

public class AddBonusDayTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) +  "</span>" +"," + "</head>";
        String bodyZi = "<body>\n" +
                 "<p>Bine ai venit in familia SharingIsCaring, sper ca experiata ta cu noi sa fie cat mai buna</p>" +
                "</body>\n";
        String footer = "<footer>\n" +
                "<p>Sharing is caring</p>" +
                "</footer>";

        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://renashome.gr/wp-content/uploads/2016/04/welcome.jpg\"  width=\"300\" height=\"300\">" + "</th>" + "<th style=\"text-align: left\">" + header + bodyZi + footer + "</th>" + "</tr>" + "</table>" + "</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getAddBonusDay().getName();
    }

}
