package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
import itss.harp.email.domain.HolidayDetailsDTO;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component

public class EditHolidayTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

//    @Autowired
//    DatabaseClient databaseClient;

    @Override
    public String write(String email, String... args) {
        HolidayDetailsDTO holidayDetails = null;
        Integer holidayType = holidayDetails.getHolidayTypeId();

        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" + "," + "</head>";
        if (holidayDetails.getRequiresJustificationFiles().equals(0) && !holidayType.equals(11)) {

            String body = "<body>\n" +
                    "<p>" + "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " tocmai ce a modificat datele" +
                    " cererii de concediu de tip: " + "<span style=\"color: #03a4ef\">" + args[1] + "</span>" +
                    ". O gasesti in sectiunea de pending din Harp si asteapta validarea ta.</p>" +
                    "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</body>\n";
            if(args[1].equals("1") || args[1].equals("5") || args[1].equals("6") || args[1].equals("7") || args[1].equals("8") || args[1].equals("9"))
                return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-1.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + "</th>" +"</tr>"+ "</table>" +"</html>";
            else return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-3.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + "</th>" +"</tr>"+ "</table>" +"</html>";

        } else {
            String body = "<body>\n" +
                    "<p> " + "<span style=\"color: #03a4ef\">" + args[0] +  "</span>" + " tocmai ce a modificat datele cererii" +
                    " de concediu de concediu de tip: " + "<span style=\"color: #03a4ef\">" + args[1] + "</span>" +
                    ". O gasesti in calendarul din Harp. </p>" +
                    "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</body>\n";
            if(args[1].equals("1") || args[1].equals("5") || args[1].equals("6") || args[1].equals("7") || args[1].equals("8") || args[1].equals("9"))
                return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-1.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + "</th>" +"</tr>"+ "</table>" +"</html>";
            else return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-3.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + "</th>" +"</tr>"+ "</table>" +"</html>";

        }
    }

    @Override
    public String getType() {
        return templateConfig.getEditHoliday().getName();
    }

}
