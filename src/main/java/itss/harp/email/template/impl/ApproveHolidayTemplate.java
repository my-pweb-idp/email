package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApproveHolidayTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" + "," + "</head>";
        String body = generateBody(args[0], args[1], args[4]);
        String comment = "<p>Ti-a lasat si un mesaj: " + args[2] + "</p>";
        String footer = generateFooter(args[3]);
        if(!args[2].isEmpty() && args[2] != null) {
            return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-3.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + comment + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
        }
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-3.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getApprovedHoliday().getName();
    }

    private String generateFooter(String arg){
        if(arg.equals("5") || arg.equals("6") || arg.equals("11")) {
            return  "<footer>\n" +
                    "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</footer>";
        } else {
            return "<footer>\n" +
                    "<p>Vacanta frumoasa, sa ne trimiti o poza. :)<p>" +
                    "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</footer>";
        }
    }

    private String generateBody(String arg1, String arg2, String arg4){
        String name;

        if (arg4 == null) {
            name = "Automat";
        } else {
            name = "de " + getFirstName(arg4) + " " + getLastName(arg4);
        }

        if (arg1.equals(arg2)) {
            return "<body>\n" +
                    "<p>Concediul tau din data " + "<span style=\"color: #03a4ef\">" + arg1 + "</span>" + " a fost aprobat " +
                    "<span style=\"color: #03a4ef\">" + name + "." + "</span>" + "</p>" +
                    "</body>\n";
        } else {
            return "<body>\n" +
                    "<p>Concediul tau din perioada " + "<span style=\"color: #03a4ef\">" + arg1 + "-" + arg2 + "</span>" +
                    " a fost aprobat " + "<span style=\"color: #03a4ef\">" + name + "." + "</span>" + "</p>" +
                    "</body>\n";
        }
    }
}
