package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component

public class DeleteHolidayTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" + "," + "</head>";
        String body = "<body>\n" +
                 "<p> " + "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " tocmai ce te anunta ca nu isi va mai lua: "
                + "<span style=\"color: #03a4ef\">" + args[1] + "</span>" +
                ". E nevoie de validarea ta pentru aceasta schimbare, eventual poate chiar il ajuti sa si-o reprogrameze. :) </p>" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</body>\n";
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-2.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() { return templateConfig.getDeleteHoliday().getName(); }

}
