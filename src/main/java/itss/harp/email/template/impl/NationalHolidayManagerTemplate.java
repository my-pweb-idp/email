package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NationalHolidayManagerTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" +  getFirstName(email) + "</span>" + "," + "</head>";
        String body = "<body>\n" +
                "<p>In putin timp urmeaza sa avem o zi libera nationala, asa ca te incurajam sa iti faci planuri " +
                "pentru ca pe data de  " + "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " vom avea liber. La fel si echipa ta. </p>" +
                "</body>\n";
        String footer = "<footer>\n" +
                "<p>Spor la planificat relaxarea :)<p>" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</footer>";
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";

    }

    @Override
    public String getType() {
        return templateConfig.getNationalHolidaysTlManager().getName();
    }
}
