package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminSubmitTemplate implements TemplateStrategy {
    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" + "," + "</head>";
        String body = generateBody(args[1], args[2], args[0]);

        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" +header + body  + "</th>" +"</tr>"+ "</table>" +"</html>";

    }

    @Override
    public String getType() { return templateConfig.getAdminSubmit().getName(); }

    private String generateBody(String arg1, String arg2, String arg0){
        if(arg1.equals(arg2)) {
            return  "<body>\n" +
                    "<p>Adminul tocmai ce a adaugat un concediu de tipul: " + "<span style=\"color: #03a4ef\">" + arg0 + "</span>" +
                    "in locul tau, in data de: " + "<span style=\"color: #03a4ef\">" + arg1+  "</span>" +
                    " </p>" +
                    "<p>Il poti vizualiza in Harp, in calendar si in sectiunea Sumar concedii.</p>" +
                    "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</body>\n";
        } else {
            return  "<body>\n" +
                    "<p>Adminul tocmai ce a adaugat un concediu de tipul: " + "<span style=\"color: #03a4ef\">" + arg0 + "</span>" +
                    "in locul tau, in perioada: " + "<span style=\"color: #03a4ef\">" + arg1 + "-" + arg2 + "</span>" +
                    " </p>" +
                    "<p>Il poti vizualiza in Harp, in calendar si in sectiunea Sumar concedii.</p>" +
                    "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</body>\n";
        }
    }
}
