package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminEditTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String body;
        String header = "<head>Buna " + getFirstName(email) + "," + "</head>";
        if (args[1].equals(args[2])) {
            body = "<body>\n" +
                    "<p>Adminul tocmai ce a modificat un concediu de tipul: " + args[0] + "in locul tau, in data de: " + args[1] +
                    " </p>" +
                    "<p>Il poti vizualiza in Harp, in calendar si in sectiunea Sumar concedii.</p>" +
                    "<p><a href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</body>\n";
        } else{
            body = "<body>\n" +
                    "<p>Adminul tocmai ce a modificat un concediu de tipul: " + args[0] + "in locul tau, in perioada: " + args[1] + "-" + args[2] +
                    " </p>" +
                    "<p>Il poti vizualiza in Harp, in calendar si in sectiunea Sumar concedii.</p>" +
                    "<p><a href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                    "</body>\n";
        }
        return "<html>" + header + body + "</html>";
    }


    @Override
    public String getType() {
        return templateConfig.getAdminDelete().getName();
    }
}
