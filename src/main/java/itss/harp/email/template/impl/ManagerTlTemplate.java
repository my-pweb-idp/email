package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
import itss.harp.email.domain.HolidayDetailsDTO;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class ManagerTlTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

//    @Autowired
//    DatabaseClient databaseClient;

    @Override
    public String write(String email, String... args) {
        HolidayDetailsDTO holidayDetails = null;
        Integer holidayType = holidayDetails.getHolidayTypeId();
        String description = holidayDetails.getDescription();

        String header = "<head>Draga " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" + "," + "</head>";
        String body;
        String footer;

        //concediu care necesita aprobare
        if (holidayDetails.getRequiresJustificationFiles().equals(0) && !holidayType.equals(11)) {
            //o singura zi
            if (args[3].equals(args[4])) {
                body = "<body>\n" +
                        "<p>" + "<span style=\"color: #03a4ef\">" +  args[0] + "</span>" + " tocmai ce te anunta ca vrea sa isi ia un concediu de tip "
                        +  "<span style=\"color: #03a4ef\">" + args[1] + "</span>" + " in data de:" +
                        "<span style=\"color: #03a4ef\">" + args[3] + "</span>" + ". Te rugam sa mergi in Harp si sa ii dai raspunsul tau." +
                        "</body>\n";
                //interval
            } else {
                body = "<body>\n" +
                        "<p>" + "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " tocmai ce te anunta ca vrea sa isi ia un concediu de tip " +
                        "<span style=\"color: #03a4ef\">" + args[1] + "</span>" + " in perioada:" +
                        "<span style=\"color: #03a4ef\">" + args[3] + " - " + args[4] + "</span>" + ". Te rugam sa mergi in Harp si sa ii dai raspunsul tau." +
                        "</body>\n";
            }
            //fara urare
            if (holidayType.equals(5) || holidayType.equals(6)) {
                if (description != null && !description.isEmpty()){
                    footer = "<footer>" +
                            "<p>Ti-a lasat si un mesaj:</p>" + description +
                            "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                            "</footer>";
                } else {
                    footer = "<footer>" +
                            "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                            "</footer>";
                }
                // cu urare
            } else {
                if (description != null && !description.isEmpty()){
                    footer = "<footer>" + "<p>Ureaza-i si vacanta frumoasa daca tot ai ajuns acolo :)</p>" +
                            "<p>Ti-a lasat si un mesaj: </p>" + description +
                            "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                            "</footer>";
                } else {
                    footer = "<footer>" + "<p>Ureaza-i si vacanta frumoasa daca tot ai ajuns acolo.</p>" +
                            "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                            "</footer>";
                }
            }
            return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-1.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";

        } else {
            if (args[3].equals(args[4])) {
                body = "<body>\n" +
                        "<p>" + "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " tocmai ce te anunta ca vrea sa isi ia un concediu de tip " +
                        "<span style=\"color: #03a4ef\">" + args[1] +  "</span>" + " in data de:" +
                        "<span style=\"color: #03a4ef\">" + args[3] + "</span>" + "." +
                        "</body>\n";
                //interval
            } else {
                body = "<body>\n" +
                        "<p>" + "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " tocmai ce te anunta ca vrea sa isi ia un concediu de tip " +
                        "<span style=\"color: #03a4ef\">" + args[1] + "</span>" + " in perioada:" +
                        "<span style=\"color: #03a4ef\">" + args[3] + " - " + args[4] + "</span>" + "." +
                        "</body>\n";
            }
            if (description != null && !description.isEmpty()){
                footer = "<footer>" +
                        "<p>Ti-a lasat si un mesaj: </p>" + description +
                        "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                        "</footer>";
            } else {
                footer = "<footer>"  +
                        "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                        "</footer>";
            }
            return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
        }
    }

    @Override
    public String getType() {
        return templateConfig.getManagerTlnotification().getName();
    }
}
