package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FiscalDateForAdminTemplate implements TemplateStrategy {
    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String body;
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) +  "</span>" + "," + "</head>";
        body = "<body>\n" +
                "Ne apropiem de finalul lunii. Poti modifica data fiscala pentru urmatoarea luna accesand Harp." +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</body>\n";
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body +  "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getFiscalDateForAdmin().getName();
    }
}
