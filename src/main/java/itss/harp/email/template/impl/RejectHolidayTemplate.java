package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RejectHolidayTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) +  "</span>" + "," + "</head>";
        String body = generateBody(args[0], args[1], args[4]);
        String footer = "<footer>\n" +
                "<p>Verifica putin cu managerul/tl-ul tau dupa ce citesti comentariul lui de mai jos: </p>" + args[2] +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</footer>";
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-2.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getRejectedHoliday().getName();
    }

    private String generateBody(String arg1, String arg2, String arg3){
        if(arg1.equals(arg2)) {
            return "<body>\n" +
                    "<p>Concediul tau din data " +"<span style=\"color: #03a4ef\">" + arg1 + "</span>" + " a fost respins " +
                    "<span style=\"color: #03a4ef\">" + getFirstName(arg3) + " " + getLastName(arg3) + "." + "</span>" + "</p>" +
                    "</body>\n";
        } else {
            return "<body>\n" +
                    "<p>Concediul tau din perioada " + "<span style=\"color: #03a4ef\">" + arg1 + "-" + arg2 + "</span>" +
                    " a fost respins de " + "<span style=\"color: #03a4ef\">" + getFirstName(arg3) + " " + getLastName(arg3) + "." + "</span>" + "</p>" +
                    "</body>\n";
        }
    }
}
