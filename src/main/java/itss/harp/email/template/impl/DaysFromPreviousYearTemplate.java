package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class DaysFromPreviousYearTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Draga " + "<span style=\"color: #03a4ef\">" + getFirstName(email) +  "</span>" + "," + "</head>";
        String body = "<body>\n" +
                "<p> Chiar a mai trecut un an! Speram ca te-ai bucurat de concediu si ai avut grija sa te relaxezi, iar acest mail te gaseste asa cum trebuie.\n" +
                " Ai incheiat anul cu " +  "<span style=\"color: #03a4ef\">" + args[0] + "</span>" + " zile de concediu," +
                " iar acum urmeaza sa mai primesti " +"<span style=\"color: #03a4ef\">" + args[1] + "</span>" +" si inca " +
                "<span style=\"color: #03a4ef\">" + args[2] + "</span>" + " ITSS de care sa profiti la maxim.\n" +
                " Numarul tau total de zile de concediu va fi " + "<span style=\"color: #03a4ef\">" + args[3] +  "</span>" +
                ". Nu uita ca proiectele reusite se fac si cu o minte relaxata si plecata in experiente frumoase.\n"+
                "</body>\n";
        String footer = "<footer>" + "<p>Happy new year si vacante frumoase!</p>" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>"
        + "</footer>";
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-Holidays_notification.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" +header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";

    }

    @Override
    public String getType() {
        return templateConfig.getDaysFromPreviousYear().getName();
    }

}
