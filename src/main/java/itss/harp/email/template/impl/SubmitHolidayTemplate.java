package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component

public class SubmitHolidayTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" +"," + "</head>";
        String body;
        if (args[1].equals(args[2])) {
            body = "<body>\n" +
                    "<p> " + "<span style=\"color: #03a4ef\">" + args[0] +  "</span> " +" tocmai ce a trimis o cerere de concediu si isi doreste sa te tina la curent." +
                    " Data aleasa este " +"<span style=\"color: #03a4ef\">" + args[1] +  "</span>" +". Daca data aleasa de el te impacteaza in vreun fel, te incurajez sa vorbesti cu el si cu managerul sau team leadul lui.\n" +
                    "Daca nu te impacteaza, ureaza-i concediu placut :) !  </p>" +
                    "</body>\n";
        } else {
            body = "<body>\n" +
                    "<p> " + "<span style=\"color: #03a4ef\">" + args[0] +  "</span> " + " tocmai ce a trimis o cerere de concediu si isi doreste sa te tina la curent." +
                    " Datele alese sunt " + "<span style=\"color: #03a4ef\">" + args[1] + " - " + args[2] +  "</span>" +". Daca datele alese de el te impacteaza in vreun fel, te incurajez sa vorbesti cu el si cu managerul sau team leadul lui.\n" +
                    "Daca nu te impacteaza, ureaza-i concediu placut :) !  </p>" +
                    "</body>\n";
        }
        String footer = "<footer>\n" +
                "<p>You've been informed :)  </p>\n" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</footer>";


        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getSubmitHoliday().getName();
    }

}
