package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompensedItssExtraTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" +  getFirstName(email) +  "</span>" + "," + "</head>";
        String body = "<body>\n" +
                "<p>In putin timp urmeaza sa avem o zi libera nationala pentru care ITSS ofera o zi in plus. " +
                "Te rugam sa alegi aceasta zi dintre datele: " +  "<span style=\"color: #03a4ef\">" +
                args[0] + " - " + args[1] + "</span>" + "</p>" +
                "</body>\n";
        String footer = "<footer>\n" +
                "<p>Note: Daca iti iei un concediu in perioada respectiva, nu uita sa iti iei prima data ziua in plus oferita de ITSS. " +
                "Pentru mai multe detalii, poti consulta ghidul utilizatorului.<p>" +
                "<p>Spor la planificat relaxarea :) </p>" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</footer>";

        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" +header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
    }


    @Override
    public String getType() {
        return templateConfig.getCompensedItssExtra().getName();
    }
}
