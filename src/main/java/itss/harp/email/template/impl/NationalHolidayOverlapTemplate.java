package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NationalHolidayOverlapTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) +  "</span>" + "," + "</head>";
        String body = "<body>\n" +
                "<p>Avem o zi nationala care se suprapune cu weekendul, in data de " +"<span style=\"color: #03a4ef\">" +
                args[0] + "</span>" +". Dupa cum stii, " +
                "noi vrem sa ne bucuram de acea zi si o compensam in timpul saptamanii." +
                " Acum tema e la tine sa iti alegi cand vei fi liber. </p>" +
                "</body>\n";
        String footer = "<footer>\n" +
                "<p>Happy free day :) </p>\n" +
                "**Ziua poate fi aleasa doar in preajma sarbatorii, asa ca fie vineri, inainte de sarbatoare sau luni, dupa sarbatoare de cele mai multe ori.\n" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/\">Poti accesa Harp AICI</a></p>" +
                "</footer>";
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/Hv-4.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getNationalHolidaysOverlapAll().getName();
    }
}
