package itss.harp.email.template.impl;

import itss.harp.email.configuration.TemplateConfig;
import itss.harp.email.template.TemplateStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Babin Sergiu
 */
@Component
public class ApprovedHolidayToOfficeManagerTemplate implements TemplateStrategy {

    @Autowired
    TemplateConfig templateConfig;

    @Override
    public String write(String email, String... args) {
        String header = "<head>Buna " + "<span style=\"color: #03a4ef\">" + getFirstName(email) + "</span>" + "," + "</head>";
        String body = generateBody(args[0], args[1], args[3], args[4], args[5]);
        String footer = generateFooter();
        return "<html>" + "<table>" + "<tr>" + "<th>" + "<img src=\"https://harp.itsmartsystems.eu/assets/images/HV-3.png\"  width=\"300\" height=\"300\">" +"</th>" + "<th style=\"text-align: left\">" + header + body + footer + "</th>" +"</tr>"+ "</table>" +"</html>";
    }

    @Override
    public String getType() {
        return templateConfig.getApprovedHolidayToOfficeManager().getName();
    }

    private String generateFooter(){
        return  "<footer>\n" +
                "<p><a style=\"color: #03a4ef\" href=\"https://harp.itsmartsystems.eu/#/vacante-echipa\">Poti accesa Harp AICI</a></p>" +
                "</footer>";
    }

    private String generateBody(String startDate, String endDate, String holidayType, String TLOrManager, String employee){
        if(startDate.equals(endDate)) {
            return "<body>\n" +
                    "<p>" + "Concediul de tip: " + "<span style=\"color: #03a4ef\">" + holidayType + "</span>" + " pentru " +
                    "<span style=\"color: #03a4ef\">" + getFirstName(employee) + " " + getLastName(employee) + "</span>" + " din data: " + "<span style=\"color: #03a4ef\">" +
                    startDate + "</span>" + " a fost aprobat de "+ "<span style=\"color: #03a4ef\">" + getFirstName(TLOrManager) + " " + getLastName(TLOrManager) + "</span>" + "." +
                    "</p>" +
                    "</body>\n";
        } else {
            return "<body>\n" +
                    "<p>" + "Concediul de tip: " + "<span style=\"color: #03a4ef\">" + holidayType + "</span>" + " pentru " +
                    "<span style=\"color: #03a4ef\">" + getFirstName(employee) + " " + getLastName(employee) + "</span>" + " din perioada" +"<span style=\"color: #03a4ef\">" +
                    startDate + "</span>" + "<span style=\"color: #03a4ef\">" + " - " + endDate + "</span>" + " a fost aprobat de "+
                    "<span style=\"color: #03a4ef\">" + getFirstName(TLOrManager) + " " + getLastName(TLOrManager) +  "</span>" + "." + "</p>" +
                    "</body>\n";
        }
    }
}
