//package itss.harp.email.dao;
//
//import itss.harp.email.response.*;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.format.annotation.DateTimeFormat;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//import java.time.LocalDate;
//import java.util.Date;
//
//@FeignClient(value = "DATABASE-API")
//public interface DatabaseClient {
//    @GetMapping("/getUsersToNotifyForNationalHoliday")
//    ResponseEntity<UsersToNotifyForNationalHolidayResponse> getUsersToNotifyForNationalHoliday(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date);
//
//    @GetMapping("/getEmailsByDays")
//    ResponseEntity<EmailByDaysResponse> getEmails();
//
//
//    @GetMapping("/getPendingDeletionEmails")
//    ResponseEntity<PendingDeletionEmailsResponse> getPendingEmails();
//
//    @GetMapping("/getPendingHolidays")
//    ResponseEntity<PendingHolidaysResponse> getPendingHolidays();
//
//    @GetMapping("/getHolidayDetails")
//    ResponseEntity<HolidayDetailsResponse> getHolidayDetails(@RequestParam Integer holidayId);
//
//    @GetMapping("/verifySpecialDays")
//    ResponseEntity<VerifySpecialDaysResponse> getVerifySpecialDays(@RequestParam String email,
//                                                                   @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
//                                                                   @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate);
//
//    @GetMapping("/getEmailsByCountry")
//    ResponseEntity<EmailsByCountryResponse> getEmailsByCountry(@RequestParam Integer countryId);
//
//    @GetMapping("/getEmailsByRole")
//    ResponseEntity<EmailsByCountryResponse> getEmailsByRole(@RequestParam Integer roleId);
//}
