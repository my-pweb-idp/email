package itss.harp.email.utils;

public final class Constants {
    public static final String INTERNAL_SERVER_ERROR = "INTERNAL SERVER ERROR";
    public static final String OK = "OK";
    public static final String HTML = "html";
    public static final String NATIONAL_HOLIDAYS = "NATIONAL_HOLIDAYS";
    public static final String DATA_NEEDS_TO_BE_COMPLETE = "DATA_NEEDS_TO_BE_COMPLETE";
    public static final String EMAIL_TRIMIS_CU_SUCCES = "EMAIL TRIMIS CU SUCCES!";
    public static final String NATIONAL = "national";
    public static final String SCHEDULED = "scheduled";
    public static final String SUBMIT = "submit";
    public static final String MINVACATION = "minimumVacation";
    public static final String PREVIOUSDAYS = "daysFromPreviousYear";
    public static final String MANAGERTL = "managerTlnotification";
    public static final String EDIT = "edit";
    public static final String EDIT_EXTRA_RECIPIENTS = "editExtraRecipients";
    public static final String DELETE = "delete";
    public static final String PENDING_DELETION = "deletion";
    public static final String PENDING_HOLIDAYS = "pendingHolidays";
    public static final String APPROVE_DELETION = "approveDeletion";
    public static final String REJECT_DELETION = "rejectDeletion";
    public static final String HOLIDAY_DELETION_ANNOUNCEMENT = "holidayDeletionAnnouncement";
    public static final String COMPENSED_ITSS_EXTRA = "compensedItssExtra";
    public static final String MANAGERTL2 = "managerTlNotification2";
    public static final String ADMIN_SUBMIT = "adminSubmit";
    public static final String ADMIN_DELETE = "adminDelete";
    public static final String ADMIN_EDIT = "adminEdit";
    public static final String MODIFIED_FISCAL_DATE = "modifiedFiscalDate";
    public static final String FISCAL_DATE_FOR_ADMIN = "fiscalDateForAdmin";
    public static final String ADD_BONUS_DAY = "addBonusDay";
    public static final String CHANGED_PROFILE_INFO = "changedProfileInfo";

    private Constants() {
    }
}
