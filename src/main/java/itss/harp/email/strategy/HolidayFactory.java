package itss.harp.email.strategy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class HolidayFactory {
    Map<String, HolidayStrategy> strategies;

    public HolidayFactory(List<HolidayStrategy> strategies) {
        this.strategies = new HashMap<>();
        strategies.forEach(s -> this.strategies.put(s.getType(), s));

    }

    public HolidayStrategy getStrategy(String key) {
        strategies.keySet().forEach(str -> log.info("STR ============== {}", str));
        return strategies.get(key);
    }
}
