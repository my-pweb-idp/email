package itss.harp.email.strategy;

import itss.harp.email.domain.EmailRequest;

public interface HolidayStrategy {

    String createEmailBody(EmailRequest emailRequest, String email);

    String getType();
}
