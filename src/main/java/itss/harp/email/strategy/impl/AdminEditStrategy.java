package itss.harp.email.strategy.impl;

import itss.harp.email.domain.EmailRequest;
import itss.harp.email.strategy.HolidayStrategy;
import itss.harp.email.template.TemplateFactory;
import itss.harp.email.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminEditStrategy implements HolidayStrategy {
    @Autowired
    TemplateFactory templateFactory;

    @Override
    public String createEmailBody(EmailRequest emailRequest, String email) {
        return templateFactory.getTemplate(emailRequest.getTemplateName()).write(email, emailRequest.getData().get("typeOfVacation"),
                emailRequest.getData().get("startDate"), emailRequest.getData().get("endDate"));
    }

    @Override
    public String getType() {
        return Constants.ADMIN_EDIT;
    }
}
