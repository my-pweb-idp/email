package itss.harp.email.strategy.impl;

import itss.harp.email.domain.EmailRequest;
import itss.harp.email.strategy.HolidayStrategy;
import itss.harp.email.template.TemplateFactory;
import itss.harp.email.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NationalHolidayStrategy implements HolidayStrategy {

    @Autowired
    TemplateFactory templateFactory;

    @Override
    public String createEmailBody(EmailRequest emailRequest, String email) {
       // log.info("this is email {}, and date: {}", email, emailRequest.getDateFrom());
        return templateFactory.getTemplate(emailRequest.getTemplateName()).write(email, emailRequest.getData().get("date"));
    }

    @Override
    public String getType() {
        return Constants.NATIONAL;
    }
}
