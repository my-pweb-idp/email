package itss.harp.email.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifySpecialDaysResponse {

    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat
    private List<LocalDate> verifySpecialDaysList;
    private List<LocalDate>  addCompensationDays;
    private List<LocalDate> nationalDays;
    private List<LocalDate> itssExtraDays;
    private List<LocalDate> compensedItssExtra;
    private HttpStatus code;
    private String message;


    public HttpStatus getCode() {
        return code;
    }
}
