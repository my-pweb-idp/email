package itss.harp.email.response;

import itss.harp.email.domain.PendingHolidaysDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PendingHolidaysResponse {
    private Integer code;
    private String message;
    private List<PendingHolidaysDTO> pendingHolidays;
}
