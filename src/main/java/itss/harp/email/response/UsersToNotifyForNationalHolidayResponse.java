package itss.harp.email.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersToNotifyForNationalHolidayResponse {
    private List<String> emails;
    private List<String> emailsTlManager;
    private Integer number;
    private Integer code;
    private String message;
}

