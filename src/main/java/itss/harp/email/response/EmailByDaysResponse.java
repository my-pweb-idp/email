package itss.harp.email.response;

import itss.harp.email.domain.AvailableDaysDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailByDaysResponse {
    private Integer code;
    private String message;
    private List<AvailableDaysDTO> emails;
}
