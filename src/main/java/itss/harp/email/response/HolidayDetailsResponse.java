package itss.harp.email.response;


import itss.harp.email.domain.HolidayDetailsDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HolidayDetailsResponse {
    private String messageStatus;
    private Integer code;
    private HolidayDetailsDTO result;


}
