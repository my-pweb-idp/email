package itss.harp.email.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailsByCountryResponse {
    private List<String> emails;
    private Integer code;
    private String message;
}
