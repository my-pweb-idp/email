package itss.harp.email.response;

import itss.harp.email.domain.PendingDeletionEmailsDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PendingDeletionEmailsResponse {

    private List<PendingDeletionEmailsDTO> emailsDTOList;
    private Integer code;
    private String message;
}
