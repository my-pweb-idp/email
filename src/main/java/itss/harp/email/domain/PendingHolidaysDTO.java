package itss.harp.email.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PendingHolidaysDTO {
    private String managerEmail;
    private String userName;
    private String holidayType;
}
