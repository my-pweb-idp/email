package itss.harp.email.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HolidayDetailsDTO {
    private Integer holidayId;
    private Integer profileId;
    private Integer countryId;
    private Integer statusId;
    private String  description;
    private Timestamp creationDate;
    private Timestamp closureDate;
    private Integer holidayTypeId;
    private Timestamp startDate;
    private Timestamp endDate;
    private Integer noOfDays;
    private String extraRecipients;
    private String email;
    private Integer requiresJustificationFiles;
}
