package itss.harp.email.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PendingDeletionEmailsDTO {

    private String supervisorEmail;
    private String userName;
    private String holidayType;
}
