//package itss.harp.email.jobs;
//
//import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
//import itss.harp.email.domain.EmailRequest;
//import itss.harp.email.response.UsersToNotifyForNationalHolidayResponse;
//import itss.harp.email.service.SendEmailService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.io.IOException;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.time.DayOfWeek;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@EnableScheduling
//@Configuration
//@Slf4j
//public class NationalHolidaysCron {
//
//    @Value("${subject.name.nationalHolidays}")
//    String subjectNationalHolidays;
//
//    @Value("${subject.name.nationalHolidaysTL}")
//    String subjectNationalHolidaysTL;
//
//    @Value("${subject.name.nationalHolidaysOverlapUser}")
//    String subjectNationalHolidaysOverlapUser;
//
//    @Value("${subject.name.nationalHolidaysOverlapTL}")
//    String subjectNationalHolidaysOverlapTL;
//
//    @Autowired
//    TemplateConfig templateConfig;
//
//    @Autowired
//    DatabaseClient databaseClient;
//
//    @Autowired
//    SendEmailService sendEmailService;
//
//    @Scheduled(cron = "${cron.nationalHoliday}")
//    public void sendEmailNationalHolidays() {
//        LocalDate dateToConvert = LocalDate.now().plusDays(14);
//        Date date = java.sql.Date.valueOf(dateToConvert);
//        try {
//
//            UsersToNotifyForNationalHolidayResponse usersToNotifyForNationalHolidayResponse =
//                    databaseClient.getUsersToNotifyForNationalHoliday(date).getBody();
//
//            DayOfWeek dateInWeekend = dateToConvert.getDayOfWeek();
//            if (dateInWeekend == DayOfWeek.SATURDAY || dateInWeekend == DayOfWeek.SUNDAY) {
//                if (usersToNotifyForNationalHolidayResponse.getCode().equals(200)) {
//                    sendEmail(usersToNotifyForNationalHolidayResponse.getEmails(), subjectNationalHolidaysOverlapUser, templateConfig.getNationalHolidaysOverlapAll());
//                    sendEmail(usersToNotifyForNationalHolidayResponse.getEmailsTlManager(), subjectNationalHolidaysOverlapTL, templateConfig.getNationalHolidaysOverlapTlManager());
//                }
//            } else if (usersToNotifyForNationalHolidayResponse.getCode().equals(200)) {
//                sendEmail(usersToNotifyForNationalHolidayResponse.getEmails(), subjectNationalHolidays, templateConfig.getNationalHolidaysAll());
//                sendEmail(usersToNotifyForNationalHolidayResponse.getEmailsTlManager(), subjectNationalHolidaysTL, templateConfig.getNationalHolidaysTlManager());
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendEmail(List<String> emails, String subject, TemplateConfig.Holiday template) throws IOException {
//        EmailRequest emailRequest = new EmailRequest();
//        emailRequest.setEmails(emails);
//        emailRequest.setContentType("html");
//        emailRequest.setSubject(subject);
//        emailRequest.setTemplateName(template.getName());
//
//        LocalDate dateToConvert = LocalDate.now().plusDays(14);
//        Map<String, String> data = new HashMap<String, String>();
//        String date =  dateToConvert.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//        data.put("date",date);
//        emailRequest.setData(data);
//        //log.info("Date from cron {}", emailRequest.getDateFrom());
//        sendEmailService.sendEmail(emailRequest, template.getType());
//    }
//
//}
