//package itss.harp.email.jobs;
//
//import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
//import itss.harp.email.domain.EmailRequest;
//import itss.harp.email.response.EmailsByCountryResponse;
//import itss.harp.email.response.UsersToNotifyForNationalHolidayResponse;
//import itss.harp.email.response.VerifySpecialDaysResponse;
//import itss.harp.email.service.SendEmailService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import static java.time.temporal.TemporalAdjusters.firstDayOfYear;
//import static java.time.temporal.TemporalAdjusters.lastDayOfYear;
//
//import java.io.IOException;
//import java.time.DayOfWeek;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@EnableScheduling
//@Configuration
//@Slf4j
//public class CompensedItssExtraCron {
//
//    @Value("${subject.name.compensedItssExtra}")
//    String subjectCompensedItssExtra;
//
//    @Autowired
//    TemplateConfig templateConfig;
//
//    @Autowired
//    DatabaseClient databaseClient;
//
//    @Autowired
//    SendEmailService sendEmailService;
//
//    @Scheduled(cron = "${cron.compensedItssExtra}")
//    public void sendEmailNationalHolidays() {
//        LocalDate now = LocalDate.now();
//        LocalDate firstDay = now.with(firstDayOfYear());
//        LocalDate lastDay = now.with(lastDayOfYear());
//        LocalDate dateToConvert = LocalDate.now().plusDays(30);
//        Date date = java.sql.Date.valueOf(dateToConvert);
//        try {
//            EmailsByCountryResponse emailsByCountryRO = databaseClient.getEmailsByCountry(3).getBody();
//            EmailsByCountryResponse emailsByCountryMD = databaseClient.getEmailsByCountry(2).getBody();
//
//            if (emailsByCountryRO.getCode().equals(200)) {
//                if (!emailsByCountryRO.getEmails().isEmpty()){
//                    VerifySpecialDaysResponse verifySpecialDaysRO =
//                        databaseClient.getVerifySpecialDays(emailsByCountryRO.getEmails().get(0), firstDay, lastDay).getBody();
//                    if (verifySpecialDaysRO.getMessage().equals("Success")) {
//                        int i = 0;
//                        for (LocalDate itssExtraDays : verifySpecialDaysRO.getItssExtraDays()) {
//                            if (itssExtraDays.compareTo(dateToConvert) == 0) {
//                                sendEmail(emailsByCountryRO.getEmails(), subjectCompensedItssExtra, templateConfig.getCompensedItssExtra(),
//                                        verifySpecialDaysRO.getCompensedItssExtra().get(i * 2), verifySpecialDaysRO.getCompensedItssExtra().get(i * 2 + 1));
//                            }
//                            i++;
//                        }
//                    }
//                }
//            }
//
//            if (emailsByCountryMD.getCode().equals(200)) {
//                if(!emailsByCountryMD.getEmails().isEmpty()){
//                    VerifySpecialDaysResponse verifySpecialDaysMD =
//                            databaseClient.getVerifySpecialDays(emailsByCountryMD.getEmails().get(0), firstDay, lastDay).getBody();
//                    if (verifySpecialDaysMD.getMessage().equals("Success")) {
//                        int i = 0;
//                        for (LocalDate itssExtraDays : verifySpecialDaysMD.getItssExtraDays()) {
//                            if (itssExtraDays.compareTo(dateToConvert) == 0) {
//                                sendEmail(emailsByCountryMD.getEmails(),subjectCompensedItssExtra, templateConfig.getCompensedItssExtra(),
//                                        verifySpecialDaysMD.getCompensedItssExtra().get(i * 2), verifySpecialDaysMD.getCompensedItssExtra().get(i * 2 + 1));
//                            }
//                            i++;
//                        }
//
//                    }
//                }
//            }
//
//
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendEmail(List<String> emails, String subject, TemplateConfig.Holiday template, LocalDate firstDate, LocalDate secondDate) throws IOException {
//        EmailRequest emailRequest = new EmailRequest();
//        emailRequest.setEmails(emails);
//        emailRequest.setContentType("html");
//        emailRequest.setSubject(subject);
//        emailRequest.setTemplateName(template.getName());
//
//
//        Map<String, String> data = new HashMap<String, String>();
//        String firstDateConverted = firstDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//        String secondDateConverted = secondDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
//        data.put("firstDate", firstDateConverted);
//        data.put("secondDate", secondDateConverted);
//        emailRequest.setData(data);
//        sendEmailService.sendEmail(emailRequest, template.getType());
//    }
//
//}
