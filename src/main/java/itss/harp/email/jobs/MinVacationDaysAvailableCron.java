//package itss.harp.email.jobs;
//
//import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
//import itss.harp.email.domain.AvailableDaysDTO;
//import itss.harp.email.domain.EmailRequest;
//import itss.harp.email.response.EmailByDaysResponse;
//import itss.harp.email.service.SendEmailService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.io.IOException;
//import java.util.*;
//
//@EnableScheduling
//@Configuration
//@Slf4j
//public class MinVacationDaysAvailableCron {
//    @Value("${subject.name.minVacationDaysAvailable}")
//    String subjectMinVacationDays;
//
//    @Autowired
//    TemplateConfig templateConfig;
//
//    @Autowired
//    DatabaseClient databaseClient;
//
//    @Autowired
//    SendEmailService sendEmailService;
//
//    @Scheduled(cron = "${cron.minVacationDaysAvailable}")
//    public void sendEmailAvailableDays() {
//        try {
//            EmailByDaysResponse emailByDaysResponse = databaseClient.getEmails().getBody();
//            if (emailByDaysResponse.getCode().equals(200)) {
//                List<AvailableDaysDTO> availableDaysDTO = emailByDaysResponse.getEmails();
//                for (AvailableDaysDTO availableDays:availableDaysDTO){
//                    List<String> emails = new ArrayList<>();
//                    emails.add(availableDays.getEmail());
//                    sendEmail(emails, subjectMinVacationDays, availableDays.getAvailableDays(),templateConfig.getMinVacationDaysAvailable());
//
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendEmail(List<String> emails, String subject,Integer availableDays, TemplateConfig.Holiday template) throws IOException {
//        EmailRequest emailRequest = new EmailRequest();
//        emailRequest.setEmails(emails);
//        emailRequest.setContentType("html");
//        emailRequest.setSubject(subject);
//        emailRequest.setTemplateName(template.getName());
//
//
//        Map<String, String> data = new HashMap<String, String>();
//        data.put("month","Octombrie");
//        data.put("availableDays",availableDays.toString());
//        emailRequest.setData(data);
//        sendEmailService.sendEmail(emailRequest, template.getType());
//    }
//
//}
