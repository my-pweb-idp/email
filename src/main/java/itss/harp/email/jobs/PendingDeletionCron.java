//package itss.harp.email.jobs;
//
//import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
//import itss.harp.email.domain.EmailRequest;
//import itss.harp.email.domain.PendingDeletionEmailsDTO;
//import itss.harp.email.response.UsersToNotifyForNationalHolidayResponse;
//import itss.harp.email.service.SendEmailService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.io.IOException;
//import java.text.SimpleDateFormat;
//import java.time.DayOfWeek;
//import java.time.LocalDate;
//import java.util.*;
//
//@EnableScheduling
//@Configuration
//@Slf4j
//public class PendingDeletionCron {
//
//    @Value("${subject.name.pendingDeletionHoliday}")
//    String subjectPendingDeletionHoliday;
//
//    @Autowired
//    TemplateConfig templateConfig;
//
//    @Autowired
//    DatabaseClient databaseClient;
//
//    @Autowired
//    SendEmailService sendEmailService;
//
//    @Scheduled(cron = "${cron.pendingDeletionHoliday}")
//    public void sendEmailPendingDeletionHoliday() {
//        try {
//        List<PendingDeletionEmailsDTO> pendingDeletionEmails = databaseClient.getPendingEmails().getBody().getEmailsDTOList();
//        for (PendingDeletionEmailsDTO pde : pendingDeletionEmails){
//            List<String> email = new ArrayList<>();
//            email.add(pde.getSupervisorEmail());
//            String userName = pde.getUserName();
//            sendEmail(email, subjectPendingDeletionHoliday,pde.getHolidayType(), templateConfig.getPendingDeletionHoliday(),userName);
//        }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendEmail(List<String> emails, String subject, String holidayType, TemplateConfig.Holiday template, String userName) throws IOException {
//        EmailRequest emailRequest = new EmailRequest();
//        emailRequest.setEmails(emails);
//        emailRequest.setContentType("html");
//        emailRequest.setSubject(subject);
//        emailRequest.setTemplateName(template.getName());
//
//        Map<String, String> data = new HashMap<String, String>();
//        data.put("userName",userName);
//        data.put("typeOfVacation", holidayType);
//        emailRequest.setData(data);
//        sendEmailService.sendEmail(emailRequest, template.getType());
//    }
//
//}
