//package itss.harp.email.jobs;
//
//import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
//import itss.harp.email.domain.EmailRequest;
//import itss.harp.email.domain.PendingHolidaysDTO;
//import itss.harp.email.response.EmailByDaysResponse;
//import itss.harp.email.response.PendingHolidaysResponse;
//import itss.harp.email.service.SendEmailService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.io.IOException;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@EnableScheduling
//@Configuration
//@Slf4j
//public class PendingHolidaysCron {
//    @Value("${subject.name.pendingHolidays}")
//    String subjectPendingHolidays;
//    @Autowired
//    TemplateConfig templateConfig;
//
//    @Autowired
//    DatabaseClient databaseClient;
//
//    @Autowired
//    SendEmailService sendEmailService;
//
//    @Scheduled(cron = "${cron.pendingHolidays}")
//    public void sendEmailPendingHolidays() {
//        try {
//            PendingHolidaysResponse pendingHolidaysResponse = databaseClient.getPendingHolidays().getBody();
//            if (pendingHolidaysResponse.getCode().equals(200)) {
//                List<PendingHolidaysDTO> pendingHolidaysDTOS = pendingHolidaysResponse.getPendingHolidays();
//                for (PendingHolidaysDTO pendingHolidays : pendingHolidaysDTOS) {
//                    sendEmail(Arrays.asList(pendingHolidays.getManagerEmail()), subjectPendingHolidays, pendingHolidays.getUserName(), pendingHolidays.getHolidayType(), templateConfig.getPendingHolidays());
//                }
//            }
//        }catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendEmail(List<String> emails, String subject, String employeeName, String holidayType, TemplateConfig.Holiday template) throws IOException {
//        EmailRequest emailRequest = new EmailRequest();
//        emailRequest.setEmails(emails);
//        emailRequest.setContentType("html");
//        emailRequest.setSubject(subject);
//        emailRequest.setTemplateName(template.getName());
//
//
//        Map<String, String> data = new HashMap<String, String>();
//        data.put("employeeName", employeeName);
//        data.put("holidayType", holidayType);
//        emailRequest.setData(data);
//        sendEmailService.sendEmail(emailRequest, template.getType());
//    }
//}
