//package itss.harp.email.jobs;
//
//import itss.harp.email.configuration.TemplateConfig;
//import itss.harp.email.dao.DatabaseClient;
//import itss.harp.email.domain.EmailRequest;
//import itss.harp.email.service.SendEmailService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//
//import java.io.IOException;
//import java.util.List;
//
//@EnableScheduling
//@Configuration
//@Slf4j
//public class FiscalDateForAdminCron {
//
//    @Value("${subject.name.fiscalDateForAdmin}")
//    String subjectFiscalDateForAdmin;
//
//    @Autowired
//    TemplateConfig templateConfig;
//
//    @Autowired
//    DatabaseClient databaseClient;
//
//    @Autowired
//    SendEmailService sendEmailService;
//
//    @Scheduled(cron = "${cron.fiscalDateForAdmin}")
//    public void sendEmailFiscalDateForAdmin(){
//        try {
//            List<String> emailsByRoleHR = databaseClient.getEmailsByRole(5).getBody().getEmails();
//            sendEmail(emailsByRoleHR, subjectFiscalDateForAdmin, templateConfig.getFiscalDateForAdmin());
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendEmail(List<String> emails, String subject, TemplateConfig.Holiday template) throws IOException {
//        EmailRequest emailRequest = new EmailRequest();
//        emailRequest.setEmails(emails);
//        emailRequest.setContentType("html");
//        emailRequest.setSubject(subject);
//        emailRequest.setTemplateName(template.getName());
//
//        //log.info("Date from cron {}", emailRequest.getDateFrom());
//        sendEmailService.sendEmail(emailRequest, template.getType());
//    }
//}
