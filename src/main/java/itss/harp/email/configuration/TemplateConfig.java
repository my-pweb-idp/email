package itss.harp.email.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "template")
public class TemplateConfig {

    Holiday nationalHolidaysAll;

    Holiday nationalHolidaysTlManager;

    Holiday nationalHolidaysOverlapAll;

    Holiday nationalHolidaysOverlapTlManager;

    Holiday requestHoliday;

    Holiday rejectedHoliday;

    Holiday approvedHoliday;

    Holiday submitHoliday;

    Holiday minVacationDaysAvailable;

    Holiday daysFromPreviousYear;

    Holiday managerTlnotification;

    Holiday editHoliday;

    Holiday editExtraRecipients;

    Holiday deleteHoliday;

    Holiday pendingDeletionHoliday;

    Holiday pendingHolidays;

    Holiday approveDeletion;

    Holiday rejectDeletion;

    Holiday holidayDeletionAnnouncement;

    Holiday compensedItssExtra;

    Holiday managerTlNotification2;

    Holiday addBonusDay;

    Holiday adminSubmit;

    Holiday adminDelete;

    Holiday adminEdit;

    Holiday modifiedFiscalDate;

    Holiday fiscalDateForAdmin;

    Holiday approvedHolidayToOfficeManager;

    Holiday changedProfileInfo;


    @Data
    public static class Holiday {
        String name;
        String type;
    }

}
