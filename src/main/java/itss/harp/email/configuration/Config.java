package itss.harp.email.configuration;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

@Configuration
//@EnableScheduling
public class Config {

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Value("${smtp.senderUserName}")
    String senderUserName;

    @Value("${smtp.senderPassword}")
    String senderPassword;

    @Value("${smtp.server.address}")
    String serverAddress;

    @Value("${smtp.server.port}")
    String serverPort;

    @Bean
    public Session getSession() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setUsername(senderUserName);
        mailSender.setPassword(senderPassword);

        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.debug", "true");
        properties.put("mail.smtp.ssl.trust", serverAddress);
        properties.put("mail.protocol.ssl.trust", serverAddress);
        properties.put("mail.properties.mail.smtp.ssl.trust", serverAddress);
        properties.put("mail.smtp.host", serverAddress);
        properties.put("mail.smtp.port", serverPort);


        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailSender.getUsername(), mailSender.getPassword());
            }
        });

        return session;
    }

}