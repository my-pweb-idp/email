//package itss.harp.email.configuration;
//
//import feign.Request;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
//import org.springframework.cloud.openfeign.EnableFeignClients;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.ConfigurableEnvironment;
//
//@Configuration
//@EnableDiscoveryClient
//@EnableFeignClients
//@ComponentScan
//public class FeignConfig {
//    @Bean
//    public static Request.Options requestOptions(ConfigurableEnvironment env) {
//        int ribbonReadTimeout = env.getProperty("ribbon.ReadTimeout", int.class, 70000);
//        int ribbonConnectionTimeout = env.getProperty("ribbon.ConnectTimeout", int.class, 60000);
//
//        return new Request.Options(ribbonConnectionTimeout, ribbonReadTimeout);
//    }
//}
