package itss.harp.email.service;

import itss.harp.email.domain.EmailRequest;
import itss.harp.email.domain.Response;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

public interface SendEmailService {

    ResponseEntity<Response> sendEmail(EmailRequest emailRequest, String type) throws IOException;

}
