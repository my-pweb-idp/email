package itss.harp.email.service.impl;

import com.sun.mail.smtp.SMTPTransport;
import itss.harp.email.domain.EmailRequest;
import itss.harp.email.domain.Response;
import itss.harp.email.service.SendEmailService;
import itss.harp.email.strategy.HolidayFactory;
import itss.harp.email.strategy.HolidayStrategy;
import itss.harp.email.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SendEmailServiceImpl implements SendEmailService {

    @Autowired
    Session session;

    @Autowired
    HolidayFactory holidayFactory;

    @Value("${smtp.senderUserName}")
    String address;

    Logger logger = LoggerFactory.getLogger(SendEmailServiceImpl.class);

    @Override
    public ResponseEntity<Response> sendEmail(EmailRequest emailRequest, String type) {

        if (isValidEmailRequest(emailRequest))
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new Response(Constants.INTERNAL_SERVER_ERROR, Constants.DATA_NEEDS_TO_BE_COMPLETE));

        try {
            emailRequest.getEmails().forEach(email -> sendEmailMessageDynamically(emailRequest, email, type));

            return ResponseEntity.status(HttpStatus.OK)
                    .body(new Response(Constants.OK, Constants.EMAIL_TRIMIS_CU_SUCCES));

        } catch (Exception ex) {
            logger.error("Interval server error ", ex);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new Response(Constants.INTERNAL_SERVER_ERROR, "SERVICE_ERROR"));
        }
    }


    private void sendEmailMessageDynamically(EmailRequest emailRequest, String email, String type) {
        try {
            MimeMessage message = new MimeMessage(session);

            if (!CollectionUtils.isEmpty(emailRequest.getCcEmails())) {
                for (String to : emailRequest.getCcEmails()) {
                    message.addRecipient(Message.RecipientType.CC, new InternetAddress(to));
                }
            }

            MimeBodyPart textPart = new MimeBodyPart();
            //de modificat
            HolidayStrategy emailStrategy = holidayFactory.getStrategy(type);
            String body = emailStrategy.createEmailBody(emailRequest, email);

            textPart.setText(body, "US-ASCII", emailRequest.getContentType());

            MimeMultipart mimeMultipart = new MimeMultipart("related");
            mimeMultipart.addBodyPart(textPart);

            message.setFrom(new InternetAddress("harp@itsmartsystems.eu", "HARP"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            message.setContent(mimeMultipart);
            message.setSubject(emailRequest.getSubject());

            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
            t.connect(session.getProperty("host"), session.getProperty("username"), session.getProperty("password"));
            t.sendMessage(message, message.getAllRecipients());
            t.close();

        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isValidEmailRequest(EmailRequest emailRequest) {
        List<Boolean> conditions = new ArrayList<>();

        conditions.add(emailRequest.getEmails().isEmpty());
        conditions.add(emailRequest.getSubject() == null || emailRequest.getSubject().isEmpty());
        conditions.add(emailRequest.getContentType() == null || emailRequest.getContentType().isEmpty());
        conditions.add(emailRequest.getTemplateName() == null || emailRequest.getTemplateName().isEmpty());

        for (boolean condition : conditions) {

            if (condition)
                return true;
        }
        return false;
    }
}
